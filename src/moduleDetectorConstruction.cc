#include "moduleDetectorConstruction.hh"
#include "moduleDetectorMessenger.hh"



#include "moduleCalorimeterSD.hh"
#include "G4AssemblyVolume.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4Trap.hh"
#include "modulelecturexml.hh"
#include "moduleRotation.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4VSolid.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

using namespace CLHEP;
//--------------------------------------------------CONSTRUCTEUR----------------------------------

moduleDetectorConstruction::moduleDetectorConstruction(char* geometry_path)
{
  // default parameter values of the calorimeter
  strcpy(geometrypath,geometry_path);
  WorldSizeYZ  = 6.  * m;
  WorldSizeX   = 6.  * m; 
  visu = 1111;
  VisAlu = 0;  
  PosX = PosX_ref;
  PosY = PosY_ref;
  ModTheta = ModTheta_ref;
  ModPhi = ModPhi_ref;
  detectorMessenger = new moduleDetectorMessenger(this);
  G4GeometryManager::GetInstance()->SetWorldMaximumExtent(WorldSizeX);
}




//--------------------------------------------------DESTRUCTEUR----------------------------------

moduleDetectorConstruction::~moduleDetectorConstruction()
{ 
  delete detectorMessenger;
}

//--------------------------------------------------CONSTRUCTION CALORIMETRE----------------------------------
G4VPhysicalVolume* moduleDetectorConstruction::Construct()
{
  DefineMaterials();
  return ConstructCalorimeter();
}

//--------------------------------------------------MATERIAUX----------------------------------
void moduleDetectorConstruction::DefineMaterials()
{ 
 //This function illustrates the possible ways to define materials
 
  G4String name, symbol;       //a=mass of a mole;
  G4double a, z, density;      //z=mean number of protons;  

  G4int ncomponents;
  G4double  fractionmass;
  G4double temperature, pressure;

//
// define Elements
//

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);

  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  a = 14.01*g/mole;
  G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);

  density = 1.43*mg/cm3;
  a = 15.999*g/mole;
  G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);

  density = 2.34*g/cm3;
  a = 10*g/mole;
  G4Element* B10  = new G4Element(name="Bore 10"  ,symbol="B10" , z= 5., a);

  density = 2.34*g/cm3;
  a = 11*g/mole;
  G4Element* B11  = new G4Element(name="Bore 11"  ,symbol="B11" , z= 5., a);

  density = 969*mg/cm3;
  a = 22.99*g/mole;
  G4Element* Na  = new G4Element(name="Sodium"  ,symbol="Na" , z= 11., a);

  density = 2.33*g/cm3;
  a = 28.09*g/mole;
  G4Element* Si = new G4Element(name="Silicon",symbol="Si" , z= 14., a );

  density = 11.35*g/cm3;
  a = 207.19*g/mole;
  G4Element* Pb = new G4Element(name="Lead" , symbol="Pb"   , z=82., a );

  density = 19.3*g/cm3;
  a = 183.85*g/mole;
  G4Element* W = new G4Element(name="Tungsten",symbol="W" , z= 74., a );

  density = 2.7*g/cm3;
  a = 26.98*g/mole;
  G4Element* Al = new G4Element(name="Aluminium",symbol="Al" , z= 13., a );

//
// define a material from elements.   case 2: mixture by fractional mass
//
  density = 2.7*g/cm3;
  G4Material* Alu = new G4Material(name="Aluminium"  , density, ncomponents=1);
  Alu->AddElement(Al, fractionmass=1);

  density = 8.28*g/cm3;
  G4Material* PbWO4 = new G4Material(name="PbWO4"  , density, ncomponents=3);
  PbWO4->AddElement(Pb, fractionmass=0.45532661);
  PbWO4->AddElement(W, fractionmass=0.40403397);
  PbWO4->AddElement(O, fractionmass=0.14063942);

  density = 2.07*g/cm3;
  G4Material* FibreAlveole = new G4Material(name="Mix pour Alveole"  , density, ncomponents=8);
  FibreAlveole->AddElement(Si, fractionmass=0.36611059*0.48);
  FibreAlveole->AddElement(O, fractionmass=0.53173295*0.48+0.33280996*0.29);
  FibreAlveole->AddElement(B10, fractionmass=0.007820091*0.48);
  FibreAlveole->AddElement(B11, fractionmass=0.0344084*0.48);
  FibreAlveole->AddElement(Na, fractionmass=0.059927964*0.48);
  FibreAlveole->AddElement(Al, fractionmass=0.23);
  FibreAlveole->AddElement(H, fractionmass= 0.13179314*0.29);
  FibreAlveole->AddElement(C, fractionmass= 0.53539691*0.29);

//
// examples of vacuum
  density     = universe_mean_density;
  pressure    = 3.e-18*pascal;
  temperature = 2.73*kelvin;
  G4Material* Vacuum = new G4Material(name="Galactic", z=1., a=1.01*g/mole,density,kStateGas,temperature,pressure);

  density = 1.290*mg/cm3;
  G4Material* Air = new G4Material("Air"  , density, 2);
  Air->AddElement(N, 0.7);
  Air->AddElement(O, 0.3);

//G4cout << *(G4Material::GetMaterialTable()) << G4endl;
//default materials of the calorimeter
  AlveoleMaterial = FibreAlveole;
  CristalMaterial = PbWO4;
  defaultMaterial = Vacuum;
  StructureMaterial = Alu;
}

//-------------------------------------------DEFINITION DES VOLUMES-------------------------------
G4VPhysicalVolume* moduleDetectorConstruction::ConstructCalorimeter()
{
//======================================================================     
//------------------------------------World-----------------------------
//======================================================================
  solidWorld = new G4Box("World", //its name
                         WorldSizeX/2,WorldSizeYZ/2,WorldSizeYZ/2);          //its size
                   
  logicWorld = new G4LogicalVolume(solidWorld, //its solid
                             defaultMaterial,//its material
                             "World");       //its name
                             
  physiWorld = new G4PVPlacement(0, //no rotation
                           G4ThreeVector(), //at (0,0,0)
                           "World", //its name
                           logicWorld, //its logical volume
                           0, //its mother  volume
                           false, //no boolean operation
                           0);              //copy number
  
  
//======================================================================
//--------------------Plaques d'aluminium
//======================================================================
#ifndef TB_2018
  G4ThreeVector   T_pl;
  moduleRotation     transf_pl;
  G4ThreeVector    colX_pl;
  G4ThreeVector    colY_pl;
  G4ThreeVector    colZ_pl;
  G4RotationMatrix   R_pl;
  G4Transform3D      orientation_pl;
  
  solidPlaqueInfCD = new G4Box("PlaqueInfCD",15.189*cm,1*mm,2*cm);      
  
  logicPlaqueInfCD = new G4LogicalVolume(solidPlaqueInfCD, StructureMaterial, "PlaqueInfCD");      
       
  T_pl.setX(1.3825*m);
  T_pl.setY(22.7727*cm);
  T_pl.setZ( -1.5185*m + 1.52 * m );
   
  transf_pl.position_rot(-167*deg,0*deg,103*deg, 90*deg,0*deg,90*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());

  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiPlaqueInfCD = new G4PVPlacement( orientation_pl, "PlaqueInfCD", logicPlaqueInfCD, physiWorld, false, 0);      

//---------------------------------------------------------------------
  solidPlaqueInfCG = new G4Box("PlaqueInfCG",15.169*cm,1*mm,1.5*cm);      
  logicPlaqueInfCG = new G4LogicalVolume(solidPlaqueInfCG, StructureMaterial, "PlaqueInfCG");      
     
  T_pl.setX(1.38414*m);
  T_pl.setY(-21.8035*cm);
  T_pl.setZ( -1.5185*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,0*deg,83*deg, 90*deg,0*deg,90*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiPlaqueInfCG = new G4PVPlacement(  orientation_pl, "PlaqueInfCG", logicPlaqueInfCG, physiWorld, false, 0);

//--------------------------------------------------------------------------
  solidPlaqueInfP = new G4Tubs("PlaqueInfP",1.25*m,1.273*m,1.342*m,-9*deg,18*deg);      
  logicPlaqueInfP = new G4LogicalVolume(solidPlaqueInfP, StructureMaterial, "PlaqueInfP");      
     
  T_pl.setX(0*m);
  T_pl.setY(0*cm);
  T_pl.setZ( -1.5185*m + 1.52 * m );
  transf_pl.position_rot(0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
#ifndef TB_2018
  physiPlaqueInfP = new G4PVPlacement(  orientation_pl, "PlaqueInfP", logicPlaqueInfP, physiWorld, false, 0); 
#endif

//--------------------------------------------------------------------------
  solidPlaqueInfP2 = new G4Tubs("PlaqueInfP2",1.273*m,1.277*m,1.342*m,-9.8999996*deg,19.6499996*deg);      
  logicPlaqueInfP2 = new G4LogicalVolume(solidPlaqueInfP2, StructureMaterial, "PlaqueInfP2");      
     
  T_pl.setX(0*m);
  T_pl.setY(0*cm);
  T_pl.setZ( -17.53 * cm + 1.52 * m );
 
  transf_pl.position_rot(0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
   R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
   orientation_pl = G4Transform3D(R_pl,T_pl);
   physiPlaqueInfP2 = new G4PVPlacement(  orientation_pl, "PlaqueInfP2", logicPlaqueInfP2, physiWorld, false, 0); 

//--------------------------------------------------------------------------
  solidPlaqueInfP3 = new G4Tubs("PlaqueInfP3",1.248*m,1.273*m,1.342*m,-9.8999996*deg,19.6499996*deg);      
  logicPlaqueInfP3 = new G4LogicalVolume(solidPlaqueInfP3, StructureMaterial, "PlaqueInfP3");      
     
  T_pl.setX(0*m);
  T_pl.setY(0*cm);
  T_pl.setZ( -17.53*cm + 1.52 * m  );
 
  transf_pl.position_rot(0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiPlaqueInfP3 = new G4PVPlacement(  orientation_pl, "PlaqueInfP3", logicPlaqueInfP3, physiWorld, false, 0);         

//--------------------------------------------------------------------------
  solidE1CD = new G4Box("E1CD",15.5395*cm,0.5*mm,35.675*cm);      
  logicE1CD = new G4LogicalVolume(solidE1CD, StructureMaterial, "E1CD");      
     
  T_pl.setX(1.37769*m);
  T_pl.setY(24.6607*cm);
  T_pl.setZ( -1.16075*m + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE1CD = new G4PVPlacement(orientation_pl, "E1CD", logicE1CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE1CG = new G4Box("E1CG",15.5373*cm,0.5*mm,35.675*cm);      
  logicE1CG = new G4LogicalVolume(solidE1CG, StructureMaterial, "E1CG");      
     
  T_pl.setX(1.37965*m);
  T_pl.setY(-23.5444*cm);
  T_pl.setZ( -1.16075*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE1CG = new G4PVPlacement(orientation_pl, "E1CG", logicE1CG, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE1CN = new G4Cons("E1CN",1.27511*m,1.28591*m, //rmin1,rmax1
                                1.54319*m,1.554*m, //rmin1,rmax1
                                5.3405*cm, //dz
                               -9.3999996*deg,19.25*deg); //startphi,deltaphi     

  logicE1CN = new G4LogicalVolume( solidE1CN, StructureMaterial, "E1CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -87.7717*cm + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE1CN = new G4PVPlacement(orientation_pl, "E1CN", logicE1CN, physiWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE1EC = new G4Tubs("E1EC", 1.2444*m, 1.2446*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicE1EC = new G4LogicalVolume(solidE1EC, StructureMaterial, "E1EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE1EC = new G4PVPlacement(orientation_pl, "E1EC", logicE1EC, physiWorld, false, 0 );        
 
//--------------------------------------------------------------------------
  solidE2CD = new G4Box("E2CD",14.6906*cm,0.5*mm,14.4327*cm);      
  logicE2CD = new G4LogicalVolume(solidE2CD, StructureMaterial, "E2CD");      
     
  T_pl.setX(1.36942*m);
  T_pl.setY(24.4698*cm);
  T_pl.setZ( -65.9673*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE2CD = new G4PVPlacement(orientation_pl, "E2CD", logicE2CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE2CG = new G4Box("E2CG",14.6885*cm,0.5*mm,14.4327*cm);      
  logicE2CG = new G4LogicalVolume(solidE2CG, StructureMaterial, "E2CG");      
     
  T_pl.setX(1.37122*m);
  T_pl.setY(-23.441*cm);
  T_pl.setZ( -65.9673*cm + 1.52 * m);
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE2CG = new G4PVPlacement(orientation_pl, "E2CG", logicE2CG, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE2CN = new G4Cons("E2CN",1.27511*m,1.28155*m, //rmin1,rmax1
                        1.51585*m,1.5223*m, //rmin1,rmax1
                        9.5142*cm, //dz
                       -9.3999996*deg,19.25*deg); //startphi,deltaphi     
  logicE2CN = new G4LogicalVolume( solidE2CN, StructureMaterial, "E2CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -30.0812*cm + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE2CN = new G4PVPlacement(orientation_pl, "E2CN", logicE2CN, physiWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE2EC = new G4Tubs("E2EC", 1.2446*m, 1.2476*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicE2EC = new G4LogicalVolume(solidE2EC, StructureMaterial, "E2EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE2EC = new G4PVPlacement(orientation_pl, "E2EC", logicE2EC, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE3CD = new G4Box("E3CD",13.9504*cm,0.5*mm,16.9173*cm);      
  logicE3CD = new G4LogicalVolume(solidE3CD, StructureMaterial, "E3CD");      
     
  T_pl.setX(1.36221*m);
  T_pl.setY(24.3032*cm);
  T_pl.setZ( -34.6173*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());

  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE3CD = new G4PVPlacement(orientation_pl, "E3CD", logicE3CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE3CG = new G4Box("E3CG",13.9483*cm,0.5*mm,16.9173*cm);      
  logicE3CG = new G4LogicalVolume(solidE3CG, StructureMaterial, "E3CG");      
     
  T_pl.setX(1.36387*m);
  T_pl.setY(-23.3508*cm);
  T_pl.setZ( -34.6173*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());

  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE3CG = new G4PVPlacement(orientation_pl, "E3CG", logicE3CG, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE3CN = new G4Cons("E3CN",1.27511*m,1.28022*m, //rmin1,rmax1
                          1.48239*m,1.4875*m, //rmin1,rmax1
                          13.059*cm, //dz
                          -9.3999996*deg,19.25*deg); //startphi,deltaphi     
  logicE3CN = new G4LogicalVolume( solidE3CN, StructureMaterial, "E3CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( 40.7902*cm + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE3CN = new G4PVPlacement(orientation_pl, "E3CN", logicE3CN, physiWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE3EC = new G4Tubs("E3EC", 1.2476*m, 1.2478*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicE3EC = new G4LogicalVolume(solidE3EC, StructureMaterial, "E3EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE3EC = new G4PVPlacement(orientation_pl, "E3EC", logicE3EC, physiWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE4CD = new G4Box("E4CD",13.0985*cm,0.5*mm,16.3281*cm);      
  logicE4CD = new G4LogicalVolume(solidE4CD, StructureMaterial, "E4CD");      
     
  T_pl.setX(1.35391*m);
  T_pl.setY(24.1116*cm);
  T_pl.setZ( -1.37194*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE4CD = new G4PVPlacement(orientation_pl, "E4CD", logicE4CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE4CG = new G4Box("E4CG",13.0965*cm,0.5*mm,16.3281*cm);      
  logicE4CG = new G4LogicalVolume(solidE4CG, StructureMaterial, "E4CG");      
     
  T_pl.setX(1.35542*m);
  T_pl.setY(-23.2469*cm);
  T_pl.setZ( -1.37194*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE4CG = new G4PVPlacement(orientation_pl, "E4CG", logicE4CG, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE4CN = new G4Cons("E4CN",1.27511*m,1.27738*m, //rmin1,rmax1
                                1.45193*m,1.4542*m, //rmin1,rmax1
                                16.3281*cm, //dz
                                -9.3999996*deg,19.25*deg);  //startphi,deltaphi     
  logicE4CN = new G4LogicalVolume( solidE4CN, StructureMaterial, "E4CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( 1.32974*m + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE4CN = new G4PVPlacement(orientation_pl, "E4CN", logicE4CN, physiWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE4EC = new G4Tubs("E4EC", 1.2478*m, 1.248*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicE4EC = new G4LogicalVolume(solidE4EC, StructureMaterial, "E4EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE4EC = new G4PVPlacement(orientation_pl, "E4EC", logicE4EC, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE5CD = new G4Box("E5CD",12.2115*cm,0.5*mm,21.2719*cm);      
  logicE5CD = new G4LogicalVolume(solidE5CD, StructureMaterial, "E5CD");      
     
  T_pl.setX(1.34526*m);
  T_pl.setY(23.9121*cm);
  T_pl.setZ( 36.2281*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE5CD = new G4PVPlacement(orientation_pl, "E5CD", logicE5CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE5CG = new G4Box("E5CG",12.2097*cm,0.5*mm,21.2719*cm);      
  logicE5CG = new G4LogicalVolume(solidE5CG, StructureMaterial, "E5CG");      
     
  T_pl.setX(1.34662*m);
  T_pl.setY(-23.1389*cm);
  T_pl.setZ( 36.2281*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE5CG = new G4PVPlacement(orientation_pl, "E5CG", logicE5CG, physiWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE6CD = new G4Box("E6CD",11.3485*cm,0.5*mm,20.3906*cm);      
  logicE6CD = new G4LogicalVolume(solidE6CD, StructureMaterial, "E6CD");      
     
  T_pl.setX(1.33686*m);
  T_pl.setY(23.7179*cm);
  T_pl.setZ( 77.8906*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE6CD = new G4PVPlacement(orientation_pl, "E6CD", logicE6CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE6CG = new G4Box("E6CG",11.3468*cm,0.5*mm,20.3906*cm);      
  logicE6CG = new G4LogicalVolume(solidE6CG, StructureMaterial, "E6CG");      

  T_pl.setX(1.33805*m);
  T_pl.setY(-23.0337*cm);
  T_pl.setZ( 77.8906*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE6CG = new G4PVPlacement(orientation_pl, "E6CG", logicE6CG, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE7CD = new G4Box("E7CD",10.5446*cm,0.5*mm,9.68438*cm);      
  logicE7CD = new G4LogicalVolume(solidE7CD, StructureMaterial, "E7CD");      
     
  T_pl.setX(1.32902*m);
  T_pl.setY(23.5371*cm);
  T_pl.setZ( 1.07966*m + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE7CD = new G4PVPlacement(orientation_pl, "E7CD", logicE7CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE7CG = new G4Box("E7CG",10.543*cm,0.5*mm,9.68438*cm);      
  logicE7CG = new G4LogicalVolume(solidE7CG, StructureMaterial, "E7CG");      
     
  T_pl.setX(1.33007*m);
  T_pl.setY(-22.9357*cm);
  T_pl.setZ( 1.07966*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE7CG = new G4PVPlacement(orientation_pl, "E7CG", logicE7CG, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE8CD = new G4Trap("E8CD", 17.35*cm,
                                 16.90296*deg, 180*deg,
                                 0.5*mm, 10.5446*cm, 10.5446*cm,
                                 0*deg,
                                 0.5*mm, 0.000001*mm, 0.000001*mm,
                                 0*deg );      

  logicE8CD = new G4LogicalVolume(solidE8CD, StructureMaterial, "E8CD");      
     
  T_pl.setX(1.38039*m);
  T_pl.setY(24.7231*cm);
  T_pl.setZ( 1.35*m + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE8CD = new G4PVPlacement(orientation_pl, "E8CD", logicE8CD, physiWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE8CG = new G4Trap("E8CG", 17.35*cm,
                                 16.90053*deg, 180*deg,
                                 0.5*mm, 10.543*cm, 10.543*cm,
                                 0*deg,
                                 0.5*mm, 0.000001*mm, 0.0000001*mm,
                                 0*deg );      
  logicE8CG = new G4LogicalVolume(solidE8CG, StructureMaterial, "E8CG");      
     
  T_pl.setX(1.3824*m);
  T_pl.setY(-23.5782*cm);
  T_pl.setZ( 1.35*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  orientation_pl = G4Transform3D(R_pl,T_pl);
  physiE8CG = new G4PVPlacement(orientation_pl, "E8CG", logicE8CG, physiWorld, false, 0 );
#endif

//======================================================================                           
// --------------------Cristaux et alv�oles
//======================================================================
  lecturexml essai(geometrypath);
  char ligne[1024];
  G4RotationMatrix Ra;
  G4RotationMatrix RAl;
  G4RotationMatrix Rc;
  G4RotationMatrix Rtot;
  G4ThreeVector Ta;
  G4ThreeVector TAl;
  G4ThreeVector Ttot;
  G4Transform3D    transf;
  moduleRotation   transf2;
  G4Transform3D    transfAl;
  G4ThreeVector  colX;
  G4ThreeVector  colY;
  G4ThreeVector  colZ;
  G4ThreeVector  colXAl;
  G4ThreeVector  colYAl;
  G4ThreeVector  colZAl;
  int num_al;

  char xml_file[200];
#ifdef TB_2018
  sprintf(xml_file,"%s/alveoles_2018.xml",geometrypath);
#else
  sprintf(xml_file,"%s/alveoles.xml",geometrypath);
#endif

  ptrGeo = fopen(xml_file,"rt");
  int j=0;

  for(int n = 0; n<nb_alveola ; n++)
  {
// Read next alveola name and geometry (G4Trap parameters)
    essai.fichiercristal(ptrGeo);
  
#ifdef TB_2018
    sprintf(xml_file,"%s/ecal_2018.xml",geometrypath);
#else
    sprintf(xml_file,"%s/ecal2.xml",geometrypath);
#endif
    ptrAl = fopen(xml_file,"rt");

    for(int i = 0; i<alveola_stack ; i++)
    {
// Read associated alveola positions (Stack of 10 alveola in a SM)
      essai.fichierAlveole(ptrAl);
     
      TAl.setX(essai.translateAl0());
      TAl.setY(essai.translateAl1() + PosY);
      TAl.setZ(essai.translateAl2() + 1.52 * m - PosX);
      printf("Alveola %d, stack %d : %d, translation %+5.7e %+5.7e %+5.7e\n",n,i,j,essai.translateAl0(),essai.translateAl1(),essai.translateAl2() + 1.52 * m);

      transf2.position_rot(essai.angles0(),essai.angles1(),essai.angles2(),
                           essai.angles3(),essai.angles4(),essai.angles5());
      colXAl.setX(transf2.XXr());
      colXAl.setY(transf2.YXr());
      colXAl.setZ(transf2.ZXr());
      colYAl.setX(transf2.XYr());
      colYAl.setY(transf2.YYr());
      colYAl.setZ(transf2.ZYr());
      colZAl.setX(transf2.XZr());
      colZAl.setY(transf2.YZr());
      colZAl.setZ(transf2.ZZr());
      RAl = HepRotation ( colXAl , colYAl , colZAl);

#ifdef TB_2018
      //printf("Rotation : tx %.4f, px %.4f, ty %.4f, py %.4f, tz %.4f, pz %.4f\n",
      //        RAl.thetaX()/deg, RAl.phiX()/deg,RAl.thetaY()/deg, RAl.phiY()/deg,RAl.thetaZ()/deg, RAl.phiZ()/deg);
// For TB 2018, the bottom of the second alveola is parallel to horizontal
      RAl.rotateZ(ModPhi);
// And, the first column of crystals was parllel to beam line
      RAl.rotateY(ModTheta);
      //printf("Rotation : tx %.4f, px %.4f, ty %.4f, py %.4f, tz %.4f, pz %.4f\n",
      //        RAl.thetaX()/deg, RAl.phiX()/deg,RAl.thetaY()/deg, RAl.phiY()/deg,RAl.thetaZ()/deg, RAl.phiZ()/deg);
#endif
      transfAl = G4Transform3D(RAl,TAl);
      solidAlveole[j]=NULL;
      logicAlveole[j]=NULL;
      physiAlveole[j]=NULL;
      solidAlveole[j] = new G4Trap( essai.xnom(), essai.parm0(), essai.parm1(), essai.parm2(), essai.parm3(), essai.parm4(),
                                    essai.parm5(), essai.parm6(), essai.parm7(), essai.parm8(), essai.parm9(), essai.parm10());

      logicAlveole[j] = new G4LogicalVolume( solidAlveole[j], AlveoleMaterial, essai.xnom(), 0,0,0);
      physiAlveole[j] = new G4PVPlacement( transfAl,        //its position
                                           essai.xnom(),    //its name
                                           logicAlveole[j], //its logical volume
                                           physiWorld,      //its mother
                                           false,           //no boolean operat
                                           0                //copy number
                                         );                     

      // vect.push_back(physiAlveole[j]);
      j++;
    }
    fclose(ptrAl);
    fgets(ligne, 1024, ptrGeo);
  }
  fclose(ptrGeo);
  printf("Alveola definition done !\n");
  
#ifdef TB_2018
  sprintf(xml_file,"%s/cristal_2018.xml",geometrypath);
#else
  sprintf(xml_file,"%s/cristal.xml",geometrypath);
#endif
  ptrGeo = fopen(xml_file,"rt");
  
  j=0;
  for(int n = 0; n<nb_alveola*5*2 ; n++)
  {
// 170 =17*5*2 = 17 alveola types * 5 crystals per alveola * 2 R/L cystals
    int loc_alveola_type=n/10;

// Read crystal name and geometry (G4Trap parameters)
    essai.fichiercristal(ptrGeo);

#ifdef TB_2018
    sprintf(xml_file,"%s/ecal_2018.xml",geometrypath);
#else
    sprintf(xml_file,"%s/ecal2.xml",geometrypath);
#endif
    ptrOrientation = fopen(xml_file,"rt");  
// And associated position (search for crystal name in file, read translation and rotation)
    essai.fichierorientation(ptrOrientation);
    fclose(ptrOrientation);
     
    Ta.setX(essai.translate0());
    Ta.setY(essai.translate1());
    Ta.setZ(essai.translate2());
    transf2.position_rot(essai.angles0(),essai.angles1(),essai.angles2(),
                         essai.angles3(),essai.angles4(),essai.angles5());
  
    colX.setX(transf2.XXr());
    colX.setY(transf2.YXr());
    colX.setZ(transf2.ZXr());
    colY.setX(transf2.XYr());
    colY.setY(transf2.YYr());
    colY.setZ(transf2.ZYr());
    colZ.setX(transf2.XZr());
    colZ.setY(transf2.YZr());
    colZ.setZ(transf2.ZZr());
     
    Ra = HepRotation ( colX , colY , colZ);
    transf = G4Transform3D(Ra,Ta); 
       
    for(int i = 0; i<alveola_stack ; i++)
    {
      num_al = i+loc_alveola_type*alveola_stack;
     
      solidCristal[j]=NULL;
      logicCristal[j]=NULL;
      physiCristal[j]=NULL;
  
      //printf("Creating solid crystal %d : deg = %e, tolerance : %e\n",j,deg,G4GeometryTolerance::GetInstance()->GetSurfaceTolerance());
      //printf("dz=%+7.5e, theta=%+7.5e, phi=%+7.5e,\n"
      //       " dy_zm=%+7.5e, dx_ym_zm=%+7.5e, dx_yp_zm=%+7.5e, alpha_zm=%+7.5e,\n dy_zp=%+7.5e, dx_ym_zp=%+7.5e, dx_yp_zp=%+7.5e, alpha_zp=%+7.5e\n",
      //        essai.parm0(), essai.parm1(), essai.parm2(),
      //        essai.parm3(), essai.parm4(), essai.parm5(), essai.parm6(),
      //        essai.parm7(), essai.parm8(), essai.parm9(), essai.parm10());
      printf("Defining crystal j=%d, n=%d, i=%d, alveola=%d\n",j,n,i,num_al);
      solidCristal[j] = new   G4Trap( essai.xnom(), essai.parm0(), essai.parm1(), essai.parm2(), essai.parm3(), essai.parm4(),
                                      essai.parm5(),essai.parm6(), essai.parm7(), essai.parm8(), essai.parm9(), essai.parm10());
      logicCristal[j] = new G4LogicalVolume( solidCristal[j], CristalMaterial, essai.xnom(), 0,0,0);
      physiCristal[j] = new G4PVPlacement( transf,               //its position
                                           essai.xnom(),         //its name
                                           logicCristal[j],      //its logical volume
                                           physiAlveole[num_al], //its mother
                                           false,                //no boolean operat
                                           0);                   //copy number
      j++;
    }
    fgets(ligne, 1024, ptrGeo);
  }
  fclose(ptrGeo);
 
//----------------------------------------------ZONES SENSIBLES------------------------------- 

// Sensitive Detectors: Cristal
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
   
  if(!calorimeterSD)
  {
    calorimeterSD = new moduleCalorimeterSD( "CalorSD",this );
    SDman->AddNewDetector( calorimeterSD );
  }
  
  for(int k = 0; k<nb_crystal ; k++)
  {
    logicCristal[k]->SetSensitiveDetector(calorimeterSD);
  }
  
// Visualization attributes
  logicWorld->SetVisAttributes (G4VisAttributes::Invisible);
 
#ifndef TB_2018
  if(visu%10 == 0)
  {
    for (int k=0;k<500;k++)  
    {
      logicCristal[k]->SetVisAttributes (G4VisAttributes::Invisible);
    }
  }
  
  if(visu%100 - visu%10 == 0)
  {
    for (int k=500;k<900;k++)  
    {
      logicCristal[k]->SetVisAttributes (G4VisAttributes::Invisible);
    }
  }
  
  if(visu%1000 - visu%100 == 0)
  {
    for (int k=900;k<1300;k++)  
    {
      logicCristal[k]->SetVisAttributes (G4VisAttributes::Invisible);
    }
  }
  
  if(visu%10000 - visu%1000 == 0)
  {
    for (int k=1300;k<1700;k++)  
    {
      logicCristal[k]->SetVisAttributes (G4VisAttributes::Invisible);
    }
  }
#else
  if(visu == 0)
  {
    for (int k=0;k<nb_crystal;k++)  
    {
      logicCristal[k]->SetVisAttributes (G4VisAttributes::Invisible);
    }
  }
#endif
 
  
  for (int k=0;k<nb_alveola*alveola_stack;k++)  
  {
    logicAlveole[k]->SetVisAttributes (G4VisAttributes::Invisible);
  }
  
#ifndef TB_2018
  logicPlaqueInfP->SetVisAttributes (G4VisAttributes::Invisible);
  
  if(VisAlu == 0)
  {
    logicPlaqueInfCD->SetVisAttributes (G4VisAttributes::Invisible); 
    logicPlaqueInfCG->SetVisAttributes (G4VisAttributes::Invisible);
    logicPlaqueInfP2->SetVisAttributes (G4VisAttributes::Invisible);
    logicPlaqueInfP3->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE1CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE1CG->SetVisAttributes (G4VisAttributes::Invisible);
    logicE1CN->SetVisAttributes (G4VisAttributes::Invisible);
    logicE1EC->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE2CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE2CG->SetVisAttributes (G4VisAttributes::Invisible);
    logicE2CN->SetVisAttributes (G4VisAttributes::Invisible);
    logicE2EC->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE3CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE3CG->SetVisAttributes (G4VisAttributes::Invisible);
    logicE3CN->SetVisAttributes (G4VisAttributes::Invisible);
    logicE3EC->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE4CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE4CG->SetVisAttributes (G4VisAttributes::Invisible);
    logicE4CN->SetVisAttributes (G4VisAttributes::Invisible);
    logicE4EC->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE5CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE5CG->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE6CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE6CG->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE7CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE7CG->SetVisAttributes (G4VisAttributes::Invisible);
  
    logicE8CD->SetVisAttributes (G4VisAttributes::Invisible);
    logicE8CG->SetVisAttributes (G4VisAttributes::Invisible);
  
  }
#endif  

  G4VisAttributes* simpleBoxVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0));
  simpleBoxVisAtt->SetVisibility(true);

  printf("Normally, at this stage, we have created all the SM items\n");
//always return the physical World
  return physiWorld;
}

//======================================================================
//---------------------------Messengers---------------------------------
//======================================================================
void moduleDetectorConstruction::SetVisSM(G4int Choice)
{ 
  visu = Choice;       
}

void moduleDetectorConstruction::SetVisAlu(G4String Choice)
{
  if(Choice == "on") 
  {
    VisAlu = 1;
  }
  else
  {
    VisAlu = 0;
  }
}

void moduleDetectorConstruction::SetPosXY(G4String Choice)
{
  G4int iret;
  G4double loc_posx=0., loc_posy=0.;
  char cloc_unit[80];
  iret=sscanf(Choice,"%lf %lf %s",&loc_posx, &loc_posy, cloc_unit);
  //printf("Try to move the matrix by %d %e %e xx%sxx : xx%sxx\n",iret,loc_posx,loc_posy, cloc_unit, Choice.data());
  G4String loc_unit=cloc_unit;
  if(iret==2) loc_unit="cm";
  if(loc_unit=="m")
  {
    loc_posx*=m;
    loc_posy*=m;
  }
  else if(loc_unit=="cm")
  {
    loc_posx*=cm;
    loc_posy*=cm;
  }
  else if(loc_unit=="mm")
  {
    loc_posx*=mm;
    loc_posy*=mm;
  }
  PosX=PosX_ref+loc_posx;
  PosY=PosY_ref+loc_posy;
  UpdateGeometry();
}

void moduleDetectorConstruction::SetModAngles(G4String Choice)
{
  G4int iret;
  G4double loc_theta=0., loc_phi=0.;
  char cloc_unit[80];
  iret=sscanf(Choice,"%lf %lf %s",&loc_theta, &loc_phi, cloc_unit);
  //printf("Try to rotate the matrix by %d %e %e xx%sxx : xx%sxx\n",iret,loc_theta,loc_phi, cloc_unit, Choice.data());
  G4String loc_unit=cloc_unit;
  if(iret==2) loc_unit="rad";
  if(loc_unit=="deg")
  {
    loc_theta*=deg;
    loc_phi*=deg;
  }
  ModTheta=ModTheta_ref+loc_theta;
  ModPhi=ModPhi_ref+loc_phi;
  UpdateGeometry();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void moduleDetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructCalorimeter());
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
