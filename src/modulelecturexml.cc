#include "G4SystemOfUnits.hh"
#include "modulelecturexml.hh"
#include "G4ios.hh"

using namespace std;
using namespace CLHEP;
//
//--------------------------------------------------------------------
//------------------------CONSTRUCTEUR/DESTRUCTEUR-------------------------
lecturexml::lecturexml(char* geometry_path)
{
  strcpy(geometrypath,geometry_path); 
}

lecturexml::~lecturexml()
{

}

//--------------------------------------------------------------------
//------------------------TAILLE DES CRISTAUX-------------------------
void lecturexml::fichiercristal(FILE *ptrGeometrie)
{
  char str[132];
  sprintf(str,"Trapezoid name=");
  recherche(str,ptrGeometrie, 1,&nom_cristal[0]);
  
  sprintf(str,"dz=");
  parametre[0] = recherche(str,ptrGeometrie, 1);
  parametre[0] = unites(parametre[0],ptrGeometrie);

  sprintf(str,"alp1=");
  parametre[6] = recherche(str,ptrGeometrie, 1);
  parametre[6] = unites(parametre[6],ptrGeometrie);
  
  sprintf(str,"bl1=");
  parametre[4] = recherche(str,ptrGeometrie, 1); 
  parametre[4] = unites(parametre[4],ptrGeometrie);
  
  sprintf(str,"tl1=");
  parametre[5] = recherche(str,ptrGeometrie, 1);
  parametre[5] = unites(parametre[5],ptrGeometrie);
  
  sprintf(str,"h1=");
  parametre[3] = recherche(str,ptrGeometrie, 1);
  parametre[3] = unites(parametre[3],ptrGeometrie);
  
  sprintf(str,"alp2=");
  parametre[10] = recherche(str,ptrGeometrie, 1);
  parametre[10] = unites(parametre[10],ptrGeometrie);
  
  sprintf(str,"bl2=");
  parametre[8] = recherche(str,ptrGeometrie, 1);
  parametre[8] = unites(parametre[8],ptrGeometrie);
  
  sprintf(str,"tl2=");
  parametre[9] = recherche(str,ptrGeometrie, 1);
  parametre[9] = unites(parametre[9],ptrGeometrie);
  
  sprintf(str,"h2=");
  parametre[7] = recherche(str,ptrGeometrie, 1);
  parametre[7] = unites(parametre[7],ptrGeometrie);
  
  sprintf(str,"phi=");
  parametre[2] = recherche(str,ptrGeometrie, 1);
  parametre[2] = unites(parametre[2],ptrGeometrie);
  
  sprintf(str,"theta=");
  parametre[1] = recherche(str,ptrGeometrie, 1);
  parametre[1] = unites(parametre[1],ptrGeometrie);
}

//--------------------------------------------------------------------
//----------------------TRANSLATIONS/ROTATIONS------------------------
void lecturexml::fichierorientation(FILE *fd)
{
  double test = -1;
  char buff[1024];
  char cherche2[64] = "ecal:";
  char str[132], *ret;
  
  strcpy(cherche,cherche2);
  
  strcat(cherche,nom_cristal);

  while (test == -1)
  {
    test = recherche(cherche,fd, 0);
    ret=fgets(buff, 1024, fd);
  }
 
  sprintf(str,"rotations:");
  recherche(str, fd , 0 , &rotate[0] );
  sprintf(str,"/");
  rechRotation(str,fd,&rotate[0]);
  
  ret=fgets(buff, 1024, fd);
  
  sprintf(str,"x=");
  translate[0] = recherche(str,fd, 1);
  translate[0] = unites(translate[0],fd);
  
  sprintf(str,"y=");
  translate[1] = recherche(str,fd, 1);
  translate[1] = unites(translate[1],fd);
  
  sprintf(str,"z=");
  translate[2] = recherche(str,fd, 1);
  translate[2] = unites(translate[2],fd);
  
  fichierRotation(rotate);
}
 
void lecturexml::fichierAlveole(FILE *fd)
{
  double test = -1;
  char buff[1024];
  char str[132], *ret;
  
  char cherche2[64] = "ecal:";
  strcpy(cherche,cherche2);
  
  strcat(cherche,nom_cristal);
  printf("Search for alveola : %s\n",cherche);
  
  while (test == -1)
  {
    test = recherche(cherche,fd, 0);
  }
  
  ret=fgets(buff, 1024, fd);
  
  sprintf(str,"rotations:");
  recherche(str,fd, 0 , &rotateAl[0] );
  ret=fgets(rotateAl,5,fd);
  
  ret=fgets(buff, 1024, fd);
  
  sprintf(str,"x=");
  translateAl[0] = recherche(str,fd, 1);
  translateAl[0] = unites(translateAl[0],fd);
  
  sprintf(str,"y=");
  translateAl[1] = recherche(str,fd, 1);
  translateAl[1] = unites(translateAl[1],fd);
  
  sprintf(str,"z=");
  translateAl[2] = recherche(str,fd, 1);
  translateAl[2] = unites(translateAl[2],fd);

  fichierRotation(rotateAl);
}

//--------------------------------------------------------------------
//------------------------LECTURE DES UNITES--------------------------
double lecturexml::unites(double valeur,FILE *tempo)
{ 
  char *ret;
  fseek(tempo, -2 , 1); 
  compteur = 0;
  ret=fgets(car,2,tempo);
  
  while(strcmp(car,"*") != 0)
  {
   ret=fgets(car,2,tempo);
  }
  
  while(strcmp(car," ") != 0)
  {
   ret=fgets(car,2,tempo);
   compteur = compteur + 1;
  }
  
  fseek(tempo, -compteur , 1);
  ret=fgets(car,compteur-1,tempo);
  
  if (strcmp(car,"mm") == 0)  valeur = valeur *mm ;
  if (strcmp(car,"cm") == 0)  valeur = valeur *cm ;
  if (strcmp(car,"m") == 0)  valeur = valeur *m  ;
  if (strcmp(car,"mum") == 0) valeur = valeur *0.001;
  if (strcmp(car,"deg") == 0) valeur = valeur *deg;
  
  return valeur;
}

//--------------------------------------------------------------------
//------------------------RECHERCHE D'UN MOT--------------------------
double lecturexml::recherche(char* mot,FILE *tempo, int sortie)
{ 
  char *ret;
  int test = 0;
  compteur = 0;
  nb_caractere = strlen(mot);
  int compteur_car_ligne;
  double renvoi=0;
  
  ret=fgets(car,nb_caractere + 1,tempo); 
  compteur_car_ligne = 0;
  
  while(strcmp(car,mot) != 0)  
  {
    fseek(tempo, -(nb_caractere-1) , 1);
    ret=fgets(car,nb_caractere + 1,tempo);
    compteur_car_ligne++;
    if (sortie == 3)  cout<<car<<endl;
    if (compteur_car_ligne == 30) 
    {
      test = -1;
      break;
    }
  }
  
  if (sortie == 1)
  {
    fseek(tempo, 1, 1);
    ret=fgets(car,2,tempo);
    compteur= 1;
    while(strcmp(car,"*") != 0)
    {
      ret=fgets(car,2,tempo);
      compteur = compteur + 1;
    }
  
    fseek(tempo, -compteur , 1);
    ret=fgets(car,compteur,tempo);
  }
  
  if (test == -1) renvoi = -1;
  if (test != -1) renvoi = atof(car);
  if (sortie == 3) cout <<renvoi<<endl;
  return renvoi;
}

void lecturexml::recherche(char *mot,FILE *tempo, int sortie,char* texte)
{ 
  char *ret;
 
  int test = 0;
  int compteur_car_ligne;
  compteur = 0;
  nb_caractere = strlen(mot);
  
  ret=fgets(car,nb_caractere + 1,tempo);
  compteur_car_ligne = 0;
  
  while(strcmp(car,mot) != 0)
  {
    //cout<<car<<endl;
    fseek(tempo, -(nb_caractere-1) , 1);
    ret=fgets(car,nb_caractere + 1,tempo);
    compteur_car_ligne++;
    if (compteur_car_ligne == 40 ) 
    {
      test = -1;
      break;
    }
  }

  if (sortie == 1)
  {
    fseek(tempo, 1, 1);
    ret=fgets(car,2,tempo);
    compteur= 1;
    while(strcmp(car," ") != 0)
    {
      ret=fgets(car,2,tempo);
      compteur = compteur + 1;
    }
  
    fseek(tempo, -compteur , 1);
    ret=fgets(car,compteur-1,tempo);
  
    nb_caractere = strlen(car);
    for (int i=0;i<=nb_caractere+1;i++)
    texte[i] = car[i];
  }
}

void lecturexml::rechRotation(char *mot,FILE *tempo,char* texte)
{
  char *ret=NULL;
  compteur= 1;
  ret=fgets(car,2,tempo);
  
  while(strcmp(car,mot) != 0)
  {
    ret=fgets(car,2,tempo);
    compteur = compteur + 1;
  }
  
  fseek(tempo, -compteur , 1);
  ret=fgets(car,compteur-1,tempo);
  
  nb_caractere = strlen(car);
  for (int i=0;i<=nb_caractere+1;i++)
  texte[i] = car[i];
}

void lecturexml::fichierRotation(char *rot)
{
  char str[132], *ret;
  char xml_file[200];
  sprintf(xml_file,"%s/rotations2.xml",geometrypath);  
  ptrRot = fopen(xml_file,"rt");
  fseek(ptrRot, 0, 0);
  double test = -1;
  char buff[1024];

  test = recherche(rot,ptrRot, 0);
  while (test == -1)
  {
    ret=fgets(buff, 1024, ptrRot);
    test = recherche(rot,ptrRot, 0);
  }
  
  sprintf(str,"thetaX=");
  angles[3] = recherche(str,ptrRot, 1);
  angles[3] = unites(angles[3],ptrRot);

  sprintf(str,"phiX=");
  angles[0] = recherche(str,ptrRot, 1);
  angles[0] = unites(angles[0],ptrRot);
  
  sprintf(str,"thetaY=");
  angles[4] = recherche(str,ptrRot, 1);
  angles[4] = unites(angles[4],ptrRot);
  
  sprintf(str,"phiY=");
  angles[1] = recherche(str,ptrRot, 1);
  angles[1] = unites(angles[1],ptrRot);
  
  sprintf(str,"thetaZ=");
  angles[5] = recherche(str,ptrRot, 1);
  angles[5] = unites(angles[5],ptrRot);
  
  sprintf(str,"phiZ=");
  angles[2] = recherche(str,ptrRot, 1);
  angles[2] = unites(angles[2],ptrRot);
    
  fclose(ptrRot);
}

//--------------------------------------------------------------------
//----------------------TRANSFERT DES VALEURS-------------------------
string lecturexml::xnom()
{
  return nom_cristal;
}

double lecturexml::translate0()
{
  return translate[0];
}

double lecturexml::translate1()
{
  return translate[1];
}

double lecturexml::translate2()
{
  return translate[2];
}

double lecturexml::translateAl0()
{
  return translateAl[0];
}

double lecturexml::translateAl1()
{
  return translateAl[1];
}

double lecturexml::translateAl2()
{
  return translateAl[2];
}

double lecturexml::parm0()
{
  return parametre[0];
}

double lecturexml::parm1()
{
  return parametre[1];
}

double lecturexml::parm2()
{
  return parametre[2];
}

double lecturexml::parm3()
{
  return parametre[3];
}

double lecturexml::parm4()
{
  return parametre[4];
}

double lecturexml::parm5()
{
  return parametre[5];
}

double lecturexml::parm6()
{
  return parametre[6];
}

double lecturexml::parm7()
{
  return parametre[7];
}

double lecturexml::parm8()
{
  return parametre[8];
}

double lecturexml::parm9()
{
  return parametre[9];
}

double lecturexml::parm10()
{
  return parametre[10];
}

double lecturexml::angles0()
{
  return angles[0];
}

double lecturexml::angles1()
{
  return angles[1];
}

double lecturexml::angles2()
{
  return angles[2];
}

double lecturexml::angles3()
{
  return angles[3];
}

double lecturexml::angles4()
{
  return angles[4];
}

double lecturexml::angles5()
{
  return angles[5];
}

