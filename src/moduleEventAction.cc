#include "moduleEventAction.hh"

#include "moduleCalorHit.hh"
#include "moduleEventActionMessenger.hh"

#include "modulePrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"

using namespace std;
using namespace CLHEP;
typedef struct { Double_t energy,numero,d_eta,d_phi,xHodo,yHodo; } DONNEES_INIT;
Double_t donnee[nb_crystal];
DONNEES_INIT cond_init;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleEventAction::moduleEventAction(TTree *ptrOut,moduleDetectorConstruction *mdc, modulePrimaryGeneratorAction* mpg)
:calorimeterCollID(-1),drawFlag("all"),printModulo(1), eventMessenger(0),ptrtransfert(ptrOut)
{
  eventMessenger = new moduleEventActionMessenger(this);
  primaryGeneratorAction = mpg;
  detector = mdc;
  ptrtransfert->Branch( "MC_infos", &cond_init, "Energy/D:number/D:d_eta/D:d_phi/D:xHodo/D:yHodo/D");
  char bname[80];
  sprintf(bname,"Energy[%d]/D",nb_crystal);
  ptrtransfert ->Branch("crystals",donnee,bname);
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleEventAction::~moduleEventAction()
{
  delete eventMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleEventAction::BeginOfEventAction(const G4Event* evt)
{
  
  G4int evtNb = evt->GetEventID();
  if (evtNb%printModulo == 0)
  { 
    G4cout << "\n---> Begin of event: " << evtNb << G4endl;
    CLHEP::HepRandom::showEngineStatus();
  }
    
  if (calorimeterCollID==-1)
  {
    G4SDManager * SDman = G4SDManager::GetSDMpointer();
    calorimeterCollID = SDman->GetCollectionID("CalCollection");
  } 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleEventAction::EndOfEventAction(const G4Event* evt)
{
#ifdef TB_2018
// Convert G4 numbering to TB elctronic channel mapping
// We start with the bottom row of each alveola, then the top one
// And we stack alveola (7-5-6)
// Top row is not equipped with electronics [25 .. 29] (so, line 2 above)
// The VFE boards read the channels vertically in snake numbering
  G4int TB_index[nb_crystal]={
                              20, 19, 10,  9,  0,
                              29, 28, 27, 26, 25,
                              22, 17, 12,  7,  2,
                              21, 18, 11,  8,  1,
                              24, 15, 14,  5,  4,
                              23, 16, 13,  6,  3};
// View with beam in back to make relation between (posX-posY) to electronic numbering :
  G4int TB_pos[nb_crystal]=  {29, 28, 27, 26, 25,
                              20, 19, 10,  9,  0,
                              21, 18, 11,  8,  1,
                              22, 17, 12,  7,  2,
                              23, 16, 13,  6,  3,
                              24, 15, 14,  5,  4};
#endif
  G4int evtNb = evt->GetEventID();
  G4double energie_cristal[nb_crystal];    //Energie dans chaque cristal
  G4int PGC = 0;
  G4int y2 = 0;
  G4int y3 = 0;
  G4int bloc , rep;
  G4int cristal_eta;
  G4int cristal_phi;
  
  // extraction de l'�nergie des hits et calcule de l'energie deposee dans les
  //cristaux
  
  G4HCofThisEvent* HCE = evt->GetHCofThisEvent();
  moduleCalorHitsCollection* CHC = 0;
  G4int n_hit = 0;
  G4double totEAbs=0, totLAbs=0;
  
//=======================================================================
//----------------R�cup�ration de l'�nergie totale d�pos�e---------------
  if (HCE) CHC = (moduleCalorHitsCollection*)(HCE->GetHC(calorimeterCollID));

  if (CHC)
  {
    n_hit = CHC->entries();
    for (G4int i=0;i<n_hit;i++)
    {
      for (G4int j=0;j<nb_crystal;j++)
      {
        totEAbs += (*CHC)[i]->GetEdepAbs(j); 
        totLAbs += (*CHC)[i]->GetTrakAbs(j);
      }
    }
  }
   
//==================================================================
//----------------- affichage des �v�nements------------------------
  G4cout << "---> End of event: " << evtNb << G4endl;  
  G4cout << "   Cristaux: total energy: " << setw(7)
         << G4BestUnit(totEAbs,"Energy")
         << "       total track length: " << setw(7)
         << G4BestUnit(totLAbs,"Length")
         << G4endl;
    
//==================================================================
//------R�cup�ration de l'�nergie d�pos�e dans chaque cristal-------
  if (CHC)
  {
    n_hit = CHC->entries();
    for (G4int ixtal=0; ixtal<nb_crystal; ixtal++)
    {
      totEAbs=0;
      totLAbs=0;
      energie_cristal[ixtal] = 0 ;
      for (G4int i=0;i<n_hit;i++)
      {
        totEAbs += (*CHC)[i]->GetEdepAbs(ixtal); 
        totLAbs += (*CHC)[i]->GetTrakAbs(ixtal);     
      }
      energie_cristal[ixtal] = totEAbs ;
      //printf("Crystal %d : E=%7.3f GeV\n",ixtal,energie_cristal[ixtal]/GeV);
    }
  }
           
  G4cout << "\n     " << n_hit << calorimeterCollID
         << " hits are stored in moduleCalorHitsCollection." << G4endl;
  
//==================================================================
//---Conversion num�rotation Geant4 -> num�rotation Supermodule-----
  for(y3 = 0 ; y3 < nb_crystal ; y3++)
  {
    PGC = y3;
    bloc = PGC / 100;
    rep = PGC - bloc * 100;
   
#ifdef TB_2018
    y2=TB_index[y3];
#else
    if(rep < 50 )
    {
      cristal_phi =19 - (2 * (1 + rep % 10)-1);//$tef bidouille
      cristal_eta = rep / 10 + 5 * bloc; 
    }
    else
    {
      cristal_phi = 19 - 2* (rep % 10) ;//$tef bidouille
      cristal_eta = (rep - 50) / 10 + 5 * bloc;
    }
    y2 = (cristal_eta) + 85 * (cristal_phi);
#endif
    donnee[y2] = energie_cristal[y3];
  }

#ifdef TB_2018
  for(int irow=0; irow<6; irow++)
  {
    for(G4int icol=0; icol<5; icol++)
    {
      G4int i=TB_pos[irow*5+icol];
      printf("%7.3f ",donnee[i]/GeV);
    }
    printf("\n");
  }
#endif
  
//==================================================================
//---------Enregistrement des donn�es dans un fichier Root----------
  int eta_i,phi_i,numero_i;
  G4double E_i,d_eta_i,d_phi_i,xHodo_i,yHodo_i; 
  //   ptrInit_intermediaire = fopen("ENTREES/Init.txt","rt");
  //   fscanf(ptrInit_intermediaire,"%f",&E_i);
  E_i = primaryGeneratorAction->GetEnergy();
  eta_i = primaryGeneratorAction->GetposEta();
  phi_i = primaryGeneratorAction->GetposPhi();
  xHodo_i= primaryGeneratorAction->GetXHodo();
  yHodo_i= primaryGeneratorAction->GetYHodo();
  d_eta_i= primaryGeneratorAction->GetDTheta();
  d_phi_i= primaryGeneratorAction->GetDPhi();
  
  cout << "This particle has E: " << E_i << " Eta : " <<  eta_i << " Phi: " << phi_i << " Xoff : " << xHodo_i << " Yoff: " << yHodo_i << endl;

//   ptrInit_intermediaire = fopen("ENTREES/Init.txt","rt");
// //  rewind( ptrInit_intermediaire );
  
//   fscanf(ptrInit_intermediaire,"%f",&E_i);
//   fscanf(ptrInit_intermediaire,"%d",&eta_i);
//   fscanf(ptrInit_intermediaire,"%d",&phi_i);
//   fscanf(ptrInit_intermediaire,"%f",&d_eta_i);
//   fscanf(ptrInit_intermediaire,"%f",&d_phi_i);
//   fscanf(ptrInit_intermediaire,"%f",&xHodo_i);
//   fscanf(ptrInit_intermediaire,"%f",&yHodo_i);
  
//   fclose(ptrInit_intermediaire);
  
#ifdef TB_2018
// Get the target crystal from the table position :
  G4double PosX=detector->PosX;
  G4double PosY=detector->PosY;
  G4int iCol=PosX/(2.5*cm);
  if(iCol<0)iCol=0;
  if(iCol>4)iCol=4;
  G4int iRow=PosY/(2.4*cm);
  if(iRow<0)iRow=0;
  if(iRow>5)iRow=5;
  numero_i=TB_pos[iRow*5+iCol];
#else
  numero_i = (eta_i) + 85 * (phi_i) ;
#endif
  
  G4cout <<  E_i << numero_i << eta_i << phi_i << d_eta_i << d_phi_i << G4endl;  
  
  cond_init.energy = E_i ;
  cond_init.numero = numero_i;
  cond_init.d_eta=d_eta_i;
  cond_init.d_phi=d_phi_i;
  cond_init.xHodo=xHodo_i;
  cond_init.yHodo=yHodo_i;
  
  ptrtransfert->Fill();
  
//==================================================================
//-----------------------Dessin des trajectoires--------------------
  if (G4VVisManager::GetConcreteInstance())
  {
    G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
    G4int n_trajectories = 0;
    if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();

    for (G4int i=0; i<n_trajectories; i++) 
    {
      G4Trajectory* trj = (G4Trajectory*) ((*(evt->GetTrajectoryContainer()))[i]);
      if (drawFlag == "all")                                      trj->DrawTrajectory();
      else if ((drawFlag == "charged")&&(trj->GetCharge() != 0.)) trj->DrawTrajectory();
      else if ((drawFlag == "neutral")&&(trj->GetCharge() == 0.)) trj->DrawTrajectory();
    }
  }
}  
