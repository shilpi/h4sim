#include "moduleRotation.hh"





//--------------------------------------------------------------------
//------------------------CONSTRUCTEUR/DESTRUCTEUR-------------------------
//--------------------------------------------------------------------

moduleRotation::moduleRotation()
{

}

moduleRotation::~moduleRotation()
{

}



//--------------------------------------------------------------------
//-------------------------MATRIX DE ROTATION-------------------------
//--------------------------------------------------------------------

void moduleRotation::position_rot(double px , double py , double pz ,
                                  double thx, double thy , double thz )
{

  XX = sin (thx)*cos(px);
  XY = sin (thy)*cos(py);
  XZ = sin (thz)*cos(pz);
  YX = sin (thx)*sin(px);
  YY = sin (thy)*sin(py);
  YZ = sin (thz)*sin(pz);
  ZX = cos(thx);
  ZY = cos(thy);
  ZZ = cos(thz);

}

double  moduleRotation::XXr()
{
  return XX;
} 
 
double  moduleRotation::XYr()
{
  return XY;
}
 
double  moduleRotation::XZr()
{
  return XZ;
} 
 
double  moduleRotation::YXr()
{
  return YX;
}
 
double  moduleRotation::YYr()
{
  return YY;
} 
 
double  moduleRotation::YZr()
{
  return YZ;
}
 
double  moduleRotation::ZXr()
{
  return ZX;
} 
 
double  moduleRotation::ZYr()
{
  return ZY;
}
 
double  moduleRotation::ZZr()
{
  return ZZ;
}
 
