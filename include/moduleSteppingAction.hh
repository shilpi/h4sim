#ifndef moduleSteppingAction_h
#define moduleSteppingAction_h 1

#include "G4UserSteppingAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class moduleSteppingAction : public G4UserSteppingAction
{
  public:
    moduleSteppingAction();
   ~moduleSteppingAction();

    void UserSteppingAction(const G4Step*);
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
