

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef moduleCalorHit_h
#define moduleCalorHit_h 1

#include "moduleDetectorConstruction.hh"
#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class moduleCalorHit : public G4VHit
{
 public:

   moduleCalorHit();
  ~moduleCalorHit();
   moduleCalorHit(const moduleCalorHit&);
   const moduleCalorHit& operator=(const moduleCalorHit&);
   int operator==(const moduleCalorHit&) const;

   inline void* operator new(size_t);
   inline void  operator delete(void*);

   void Draw();
   void Print();
      
 public:
  
   void AddAbs(G4double de, G4double dl,G4int test) {EdepAbs[test] += de;TrackLengthAbs[test] += dl;};
                 
   G4double GetEdepAbs(int j)     { return EdepAbs[j]; };
   G4double GetTrakAbs(int j)     { return TrackLengthAbs[j]; };
   
    
 private:
  
   G4double EdepAbs[nb_crystal], TrackLengthAbs[nb_crystal];
        
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<moduleCalorHit> moduleCalorHitsCollection;

extern G4Allocator<moduleCalorHit> moduleCalorHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* moduleCalorHit::operator new(size_t)
{
  void* aHit;
  aHit = (void*) moduleCalorHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void moduleCalorHit::operator delete(void* aHit)
{
  moduleCalorHitAllocator.FreeSingle((moduleCalorHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


