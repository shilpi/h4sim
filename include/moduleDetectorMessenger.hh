
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef moduleDetectorMessenger_h
#define moduleDetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class moduleDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class moduleDetectorMessenger: public G4UImessenger
{
  public:
    moduleDetectorMessenger(moduleDetectorConstruction* );
   ~moduleDetectorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    moduleDetectorConstruction* moduleDetector;
    
    G4UIdirectory*             moduledetDir;
    G4UIcmdWithAnInteger*      Vis_SM_Cmd;
    G4UIcmdWithAString*        Vis_Alu_Cmd;
    G4UIcmdWithAString*        Set_PosXY_Cmd;
    G4UIcmdWithAString*        Set_ModAngles_Cmd;
    G4UIcmdWithoutParameter*   UpdateCmd;
    
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

