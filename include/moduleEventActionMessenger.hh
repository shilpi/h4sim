
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef moduleEventActionMessenger_h
#define moduleEventActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class moduleEventAction;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class moduleEventActionMessenger: public G4UImessenger
{
  public:
    moduleEventActionMessenger(moduleEventAction*);
   ~moduleEventActionMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    moduleEventAction*   eventAction;   
    G4UIcmdWithAString* DrawCmd;
    G4UIcmdWithAnInteger* PrintCmd;    
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
