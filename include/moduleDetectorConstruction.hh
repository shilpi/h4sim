
#ifndef moduleDetectorConstruction_h
#define moduleDetectorConstruction_h 1


#include "G4VUserDetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"

#include <vector>
 
const int max_nb_alveola=17;
#ifndef TB_2018
const int             nb_crystal = 1700;
const int             nb_alveola = 17;
const int             alveola_type[nb_alveola]= { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16};
const int             alveola_stack=10;
#else
const int             nb_crystal = 30;
const int             nb_alveola = 3;
const int             alveola_type[nb_alveola] ={7,5,6};
const int             alveola_stack=1;
#endif

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4UniformMagField;
class moduleDetectorMessenger;
class moduleCalorimeterSD;
class G4AssemblyVolume;
class G4Trap;
class G4Tubs;
class G4Cons;


class moduleDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
  
    moduleDetectorConstruction(char* geometry_path);
   ~moduleDetectorConstruction();

  public:
     
     void SetVisSM(G4int);
     void SetVisAlu(G4String);
     void SetPosXY(G4String);
     void SetModAngles(G4String);
       
     G4VPhysicalVolume* Construct();

     void UpdateGeometry();
     
  public:
  
     G4int visu;
     G4int VisAlu;
     G4double PosX, PosY, ModTheta, ModPhi;
     
     FILE *ptrGeo;
     FILE *ptrAl;
     FILE *ptrOrientation;          
     
     const G4VPhysicalVolume* GetCristal(int num__cristal) {return physiCristal[num__cristal];};
                
      
   private:                 
    
  char geometrypath[150];   
  G4Material        *AlveoleMaterial;
  G4Material        *CristalMaterial;
  G4Material        *defaultMaterial;

  const G4double     PosX_ref=0.0*mm;
  const G4double     PosY_ref=0.0*mm;
  const G4double     ModTheta_ref=21.7*CLHEP::deg;
  const G4double     ModPhi_ref=-1.*CLHEP::deg;
  G4double           WorldSizeYZ;
  G4double           WorldSizeX;
  
  G4Box             *solidWorld;      //pointer to the solid World 
  G4LogicalVolume   *logicWorld;      //pointer to the logical World
  G4VPhysicalVolume *physiWorld;      //pointer to the physical World
  
  G4Trap            *solidAlveole[nb_alveola*alveola_stack];   //pointer to the solid Alveole
  G4LogicalVolume   *logicAlveole[nb_alveola*alveola_stack];   //pointer to the logical Alveole
  G4VPhysicalVolume *physiAlveole[nb_alveola*alveola_stack];   //pointer to the physical Alveole
  
  G4Trap             *solidCristal[nb_crystal];
  G4LogicalVolume    *logicCristal[nb_crystal];      //pointer to the logical Cristal
  G4VPhysicalVolume  *physiCristal[nb_crystal];      //pointer to the physical Cristal
  
  G4Material    *StructureMaterial;
  
  G4Box             *solidPlaqueInfCD;      
  G4LogicalVolume   *logicPlaqueInfCD;      
  G4VPhysicalVolume *physiPlaqueInfCD;      
  
  G4Box             *solidPlaqueInfCG;      
  G4LogicalVolume   *logicPlaqueInfCG;      
  G4VPhysicalVolume *physiPlaqueInfCG;
  
  G4Tubs             *solidPlaqueInfP;      
  G4LogicalVolume    *logicPlaqueInfP;      
  G4VPhysicalVolume  *physiPlaqueInfP;
  
  G4Tubs             *solidPlaqueInfP2;      
  G4LogicalVolume    *logicPlaqueInfP2;      
  G4VPhysicalVolume  *physiPlaqueInfP2;
  
  G4Tubs             *solidPlaqueInfP3;      
  G4LogicalVolume    *logicPlaqueInfP3;      
  G4VPhysicalVolume  *physiPlaqueInfP3;
  
  G4Box             *solidE1CD;      
  G4LogicalVolume   *logicE1CD;      
  G4VPhysicalVolume *physiE1CD;      
  
  G4Box             *solidE1CG;      
  G4LogicalVolume   *logicE1CG;      
  G4VPhysicalVolume *physiE1CG;
  
  G4Cons             *solidE1CN;      
  G4LogicalVolume    *logicE1CN;      
  G4VPhysicalVolume  *physiE1CN;

  G4Tubs             *solidE1EC;      
  G4LogicalVolume    *logicE1EC;      
  G4VPhysicalVolume  *physiE1EC;

  G4Box             *solidE2CD;      
  G4LogicalVolume   *logicE2CD;      
  G4VPhysicalVolume *physiE2CD;      

  G4Box             *solidE2CG;      
  G4LogicalVolume   *logicE2CG;      
  G4VPhysicalVolume *physiE2CG;

  G4Cons             *solidE2CN;      
  G4LogicalVolume    *logicE2CN;      
  G4VPhysicalVolume  *physiE2CN;

  G4Tubs             *solidE2EC;      
  G4LogicalVolume    *logicE2EC;      
  G4VPhysicalVolume  *physiE2EC;

  G4Box             *solidE3CD;      
  G4LogicalVolume   *logicE3CD;      
  G4VPhysicalVolume *physiE3CD;      

  G4Box             *solidE3CG;      
  G4LogicalVolume   *logicE3CG;      
  G4VPhysicalVolume *physiE3CG;

  G4Cons             *solidE3CN;      
  G4LogicalVolume    *logicE3CN;      
  G4VPhysicalVolume  *physiE3CN;

  G4Tubs             *solidE3EC;      
  G4LogicalVolume    *logicE3EC;      
  G4VPhysicalVolume  *physiE3EC;

  G4Box             *solidE4CD;      
  G4LogicalVolume   *logicE4CD;      
  G4VPhysicalVolume *physiE4CD;      

  G4Box             *solidE4CG;      
  G4LogicalVolume   *logicE4CG;      
  G4VPhysicalVolume *physiE4CG;

  G4Cons             *solidE4CN;      
  G4LogicalVolume    *logicE4CN;      
  G4VPhysicalVolume  *physiE4CN;

  G4Tubs             *solidE4EC;      
  G4LogicalVolume    *logicE4EC;      
  G4VPhysicalVolume  *physiE4EC;
       
  G4Box             *solidE5CD;      
  G4LogicalVolume   *logicE5CD;      
  G4VPhysicalVolume *physiE5CD;      

  G4Box             *solidE5CG;      
  G4LogicalVolume   *logicE5CG;      
  G4VPhysicalVolume *physiE5CG;

  G4Box             *solidE6CD;      
  G4LogicalVolume   *logicE6CD;      
  G4VPhysicalVolume *physiE6CD;      

  G4Box             *solidE6CG;      
  G4LogicalVolume   *logicE6CG;      
  G4VPhysicalVolume *physiE6CG;

  G4Box             *solidE7CD;      
  G4LogicalVolume   *logicE7CD;      
  G4VPhysicalVolume *physiE7CD;      

  G4Box             *solidE7CG;      
  G4LogicalVolume   *logicE7CG;      
  G4VPhysicalVolume *physiE7CG;

  G4Trap            *solidE8CD;      
  G4LogicalVolume   *logicE8CD;      
  G4VPhysicalVolume *physiE8CD;      

  G4Trap            *solidE8CG;      
  G4LogicalVolume   *logicE8CG;      
  G4VPhysicalVolume *physiE8CG;


  moduleDetectorMessenger* detectorMessenger;  //pointer to the Messenger
  moduleCalorimeterSD* calorimeterSD=NULL;  //pointer to the sensitive detector

    //std::vector<G4VPhysicalVolume*> vect;
        
private:
  
   void DefineMaterials();

   G4VPhysicalVolume* ConstructCalorimeter();     
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#endif
